#!/bin/bash

# Check if MFA token code is provided as an argument
if [ -z "$1" ]; then
  echo "Please provide the MFA token code as an argument."
  exit 1
fi

# Set the MFA serial number
mfa_serial="arn:aws:iam::531302520546:mfa/Edison.Heramil"

token_duration=129600
# Generate MFA token
mfa_token_code="$1"
mfa_credentials=$(aws sts get-session-token --serial-number "$mfa_serial" --token-code "$mfa_token_code" --duration-seconds "$token_duration")

# Extract MFA credentials
access_key_id=$(echo "$mfa_credentials" | jq -r '.Credentials.AccessKeyId')
secret_access_key=$(echo "$mfa_credentials" | jq -r '.Credentials.SecretAccessKey')
session_token=$(echo "$mfa_credentials" | jq -r '.Credentials.SessionToken')

# Configure AWS CLI with MFA credentials using a profile
profile_name="mfa"
aws configure set aws_access_key_id "$access_key_id" --profile "$profile_name"
aws configure set aws_secret_access_key "$secret_access_key" --profile "$profile_name"
aws configure set aws_session_token "$session_token" --profile "$profile_name"



