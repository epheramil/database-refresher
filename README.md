# database-refresher

Working in progress

# System Requirements
    - aws account with CLI
    - mysql account
    - ansible latest version

# Getting started

Clone the repository in any server that has access on database

    - $ git clone https://gitlab.com/epheramil/database-refresher.git
    - $ ansible-galaxy collection install community.aws -f

# For AWS Access Make sure to run this script inside bash script
    MFA-Token on ex. 123456
    - $ bash bash-scripts/aws_mfa.sh 255562

# Make you own ansible vault using your own MYSQL Credentials


    - $ ansible-vault create group_vars/mysql_access.yml

            the variable should be Ex.
            mysql_user: "your_username"
            mysql_pass: "your_password"

# EDIT VAULT
    - $ ansible-vault edit group_vars/mysql_access.yml

# Things that should be change before running the playbook

    - $ nano database-refresher/group_vars/localMachine
        Edit the with_nested loop name mysql_sanitization_item_list base on the request Sanitization tier used
        mysql_sanitization_item_list:
        - { dir: "ALL", name: "sanitize.sql" }
        - { dir: "ALL", name: "table_truncation.sql" }
        - { dir: "STG1",  name: "Questions.sql"}
        - { dir: "STG1",  name: "sanitize.sql"} 

# How to run ansible

### There are two ways to run the playbook
- First do the manual deplopment:
    =

    - $ ansible-playbook  -i host.ini  playbookname.yml  --ask-vault-pass -vvv

- Second do in bash with arguments 
    =

    - bash exec-refresh_playbook.sh playbook/$"{{ name of playbook }}"

- Actual running of the code example running on MYSQL playbook:
    =
    - bash exec-refresh_playbook.sh playbook/aws_snapshot_restore.yml
    - bash exec-refresh_playbook.sh playbook/aws_mysql_playbook.yml
    - bash exec-refresh_playbook.sh playbook/mysql_playbook.yml


    

# Documentation

##  aws_snapshot_restore.yml - 

        * This playbook will snapshot the Production instance into latest state that will be used for a refresh both KPS and M1.
        * Gather information on base on AWS Rds tier you want to refresh.
        * Use all the information gather to create new refresh instance, this will include security group, allocated_storage and etc.

## aws_mysql_playbook.yml -

        * All the process on mysql Backup, restore, pull git sanitization script and sanitized data
## aws_replace_rename.yml

        * Rename old RDS Tier instance and replace with the new refresh DB tier


